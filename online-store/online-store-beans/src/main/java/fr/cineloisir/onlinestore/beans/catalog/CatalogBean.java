package fr.cineloisir.onlinestore.beans.catalog;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import fr.cineloisir.onlinestore.entity.catalog.Article;

/**
 * Session Bean implementation class CatalogBean
 */
@Stateless
@SuppressWarnings("unchecked")
public class CatalogBean implements CatalogBeanRemote {

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	private int nbTop = 5;

	/**
	 * Default constructor.
	 */
    public CatalogBean() {
		// Nothing to do
    }

	public List<Article> findArticlesByGenre(String genre) {
		Query query = manager
				.createQuery("SELECT a FROM Article a WHERE a.video.genre LIKE :genre");
		query.setParameter("genre", genre);
		return query.getResultList();
	}

	public List<Article> findBestSellingArticles() {
		Query query = manager
				.createQuery("SELECT a FROM Article a ORDER BY a.nbSales DESC");
		query.setFirstResult(0);
		query.setMaxResults(nbTop);
		return query.getResultList();
	}

	public List<Article> findNewArticles() {
		Query query = manager
				.createQuery("SELECT a FROM Article a ORDER BY a.catalogueDate DESC");
		query.setFirstResult(0);
		query.setMaxResults(nbTop);
		return query.getResultList();
	}

	public List<Article> findArticlesByDirector(String director) {
		Query query = manager
				.createQuery("SELECT a FROM Article a WHERE a.video.Director=:director");
		query.setParameter("director", director);
		return query.getResultList();
	}

	public List<Article> findArticlesByTitle(String title) {
		Query query = manager
				.createQuery("SELECT a FROM Article a WHERE a.video.title=:title");
		query.setParameter("title", title);
		return query.getResultList();
	}

	public List<Article> findArticlesByReleaseYear(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, 1, 1);
		Date startDate = calendar.getTime();
		calendar.set(year, 12, 31);
		Date endDate = calendar.getTime();
		Query query = manager
				.createQuery("SELECT a FROM Article a WHERE a.catalogueDate BETWEEN :startDate and :endDate");
		query.setParameter("startDate", startDate, TemporalType.DATE);
		query.setParameter("endDate", endDate, TemporalType.DATE);

		return query.getResultList();
	}

	public void setNbTop(int nbTop) {
		this.nbTop = nbTop;
	}

	@Override
	public List<Article> findAllArticles() {
		Query query = this.manager.createQuery("SELECT a FROM Article a");
		return query.getResultList();
	}

}
