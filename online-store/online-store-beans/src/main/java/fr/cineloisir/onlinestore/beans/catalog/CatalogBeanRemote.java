package fr.cineloisir.onlinestore.beans.catalog;

import java.util.List;

import javax.ejb.Remote;

import fr.cineloisir.onlinestore.entity.catalog.Article;

@Remote
public interface CatalogBeanRemote {
	/**
	 * Finds films of the specified genre
	 * 
	 * @param genre
	 *            the genre
	 * @return the list of films of the specified genre
	 */
	List<Article> findArticlesByGenre(String genre);

	/**
	 * Finds the best selling articles
	 * 
	 * @return the list of best selling articles
	 */
	List<Article> findBestSellingArticles();

	/**
	 * Finds the last films added to the catalog
	 * 
	 * @return the list of last articles added to the catalog
	 */
	List<Article> findNewArticles();

	/**
	 * Finds the films directed by the specified director
	 * 
	 * @param director
	 *            the director
	 * @return the list of films directed by the specified director
	 */
	List<Article> findArticlesByDirector(String director);

	/**
	 * Finds articles with the specified title
	 * 
	 * @param title
	 *            the title
	 * @return the list of articles with the specified title
	 */
	List<Article> findArticlesByTitle(String title);

	/**
	 * Finds the films released in the specified year
	 * 
	 * @param year
	 *            the year
	 * @return the list of films released in the specified year
	 */
	List<Article> findArticlesByReleaseYear(int year);

	/**
	 * Finds all the films
	 * 
	 * @param year
	 *            the year
	 * @return the list of all films
	 */
	List<Article> findAllArticles();

	/**
	 * Sets the number of articles returned by methods : findNewArticles and
	 * findBestSellingArticles
	 * 
	 * @param nbTop
	 *            The number of articles
	 */
	void setNbTop(int nbTop);

}
