package fr.cineloisir.onlinestore.beans.customers;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import fr.cineloisir.onlinestore.entity.customer.Customer;

/**
 * Session Bean implementation class CustomerBean
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CustomerBean implements CustomerBeanRemote {

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@Inject
	private UserTransaction transaction;

	/**
	 * Default constructor.
	 */
	public CustomerBean() {
		// Nothing to do
	}

	@Override
	public Customer authenticate(String userName, String password) {
		// TODO authentication with JAAS
		Customer customer;
		Query query = this.manager
				.createQuery("SELECT c FROM Customer c WHERE c.login=:userName AND c.password=:password");
		query.setParameter("userName", userName);
		query.setParameter("password", password);
		try {
			customer = (Customer) query.getSingleResult();
		} catch (NoResultException exception) {
			customer = null;
		}
		return customer;
	}

	@Override
	public void createCustomer(Customer customer) throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		manager.persist(customer);
		this.transaction.commit();

	}

	@Override
	public void updateAdresseCustomer(Customer customer, String city,
			String street1, String street2, int zipCode) throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		customer.setCity(city);
		customer.setStreet1(street1);
		customer.setStreet2(street2);
		customer.setZipCode(zipCode);
		manager.merge(customer);
		this.transaction.commit();
	}

	@Override
	public void updateEmailCustomer(Customer customer, String email)
			throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		customer.setMail(email);
		manager.merge(customer);
		this.transaction.commit();
	}

}
