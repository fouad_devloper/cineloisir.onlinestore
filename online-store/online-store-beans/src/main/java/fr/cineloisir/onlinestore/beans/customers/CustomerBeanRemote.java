package fr.cineloisir.onlinestore.beans.customers;

import javax.ejb.Remote;

import fr.cineloisir.onlinestore.entity.customer.Customer;

@Remote
public interface CustomerBeanRemote {

	/**
	 * Authenticate a user and return the authenticated Subject.
	 * 
	 * @param userName
	 *            The user name of the user to authenticate
	 * @param password
	 *            The password for that user
	 * @return the Subject representing the authenticated user
	 */

	Customer authenticate(String userName, String password);

	/**
	 * adds the specified customer to client's list
	 * 
	 * @param customer
	 *            the customer to be added
	 */
	void createCustomer(Customer customer) throws Exception;

	/**
	 * updates the customer address
	 * 
	 * @param customerPseudo
	 *            the customer pseudo
	 * @param city
	 *            the new address
	 * @param street1
	 *            TODO
	 * @param street2
	 *            TODO
	 * @param zipCode
	 *            TODO
	 */
	void updateAdresseCustomer(Customer customer, String city,
			String street1, String street2, int zipCode) throws Exception;

	/**
	 * updates the customer email
	 * 
	 * @param customerPseudo
	 *            the customer pseudo
	 * @param address
	 *            the new email
	 */
	void updateEmailCustomer(Customer customer, String email)
			throws Exception;

}
