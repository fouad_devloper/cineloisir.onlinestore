package fr.cineloisir.onlinestore.beans.shoppingcart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import fr.cineloisir.onlinestore.entity.catalog.Article;

/**
 * Session Bean implementation class ShoppingCartBean
 */
@Stateful
@TransactionManagement(TransactionManagementType.BEAN)
public class ShoppingCartBean implements ShoppingCartBeanRemote {
	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@Inject
	UserTransaction transaction;

	private final Map<Article, Integer> cartItems = new HashMap<Article, Integer>();

	/**
	 * Default constructor.
	 */
	public ShoppingCartBean() {
		// Nothing to do
	}

	public void addItem(Article article, Integer quantity) {
		this.cartItems.put(article, quantity);
	}

	public void removeItem(Article item) {
		this.cartItems.remove(item);
	}

	public void empty() {
		this.cartItems.clear();
	}

	public void updateQuantityItem(Article item, Integer quantity) {
		addItem(item, quantity);
	}

	public int getTotalCost() {
		Integer sum = 0;
		for (Article article : this.cartItems.keySet()) {
			sum += article.getUnitCost() * this.cartItems.get(article);
		}
		return sum;
	}

	public List<Article> getCartItems() {
		return new ArrayList<Article>(this.cartItems.keySet());
	}

	public void validateCart() throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		int quantity;
		for (Article article : this.cartItems.keySet()) {
			quantity = this.cartItems.get(article);
			article.setNbSales(article.getNbSales() + quantity);
			article.setNbUnits(article.getNbUnits() - quantity);
			this.manager.merge(article);
		}
		this.transaction.commit();

		// TODO send mail to client

	}

}
