package fr.cineloisir.onlinestore.beans.shoppingcart;

import java.util.List;

import javax.ejb.Remote;

import fr.cineloisir.onlinestore.entity.catalog.Article;

@Remote
public interface ShoppingCartBeanRemote {
	/**
	 * Adds the specified article to the cart
	 * 
	 * @param item
	 *            the article to be added
	 */
	void addItem(Article item, Integer quantity);

	/**
	 * Removes the specified article from the cart
	 * 
	 * @param item
	 *            the article to be removed
	 */
	void removeItem(Article item);

	/**
	 * Removes all articles from the cart
	 * 
	 */
	void empty();

	/**
	 * Updates the quantity of the specified article
	 * 
	 * @param item
	 *            the article
	 * @param quantity
	 *            the new quantity
	 */
	void updateQuantityItem(Article item, Integer quantity);

	/**
	 * Computes the shopping cart total cost
	 * 
	 * @return the total cost
	 */
	int getTotalCost();

	/**
	 * Returns the list of shopping cart items
	 * 
	 * @return the list of shopping cart items
	 */
	List<Article> getCartItems();

	/**
	 * Validates cart
	 */
	void validateCart() throws Exception;

}
