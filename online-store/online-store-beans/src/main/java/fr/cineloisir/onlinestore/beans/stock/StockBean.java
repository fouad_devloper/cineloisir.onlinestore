package fr.cineloisir.onlinestore.beans.stock;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import fr.cineloisir.onlinestore.entity.catalog.Article;
import fr.cineloisir.onlinestore.entity.catalog.Video;

/**
 * Session Bean implementation class StockBean
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@WebService(endpointInterface = "fr.cineloisir.onlinestore.beans.stock.StockBeanRemote")
public class StockBean implements StockBeanRemote {

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@Inject
	private UserTransaction transaction;
	/**
     * Default constructor. 
     */
    public StockBean() {
		// Nothing to do
    }
	
	public void addArticle(Article article) throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		this.manager.persist(article);
		this.transaction.commit();

	}

	public void addVideo(Video video) throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		this.manager.persist(video);
		this.transaction.commit();
	}

	public void updateQuantityArticle(Article article, int qte)
			throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		article.setNbUnits(qte);
		this.manager.merge(article);
		this.transaction.commit();
	}

}
