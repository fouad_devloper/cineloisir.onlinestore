package fr.cineloisir.onlinestore.beans.stock;

import javax.ejb.Remote;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import fr.cineloisir.onlinestore.entity.catalog.Article;
import fr.cineloisir.onlinestore.entity.catalog.Video;

@Remote
@WebService
@SOAPBinding(style = Style.RPC)
public interface StockBeanRemote {
	/**
	 * adds the specified article to Stock
	 * 
	 * @param article
	 *            the article to be added
	 */
	void addArticle(Article article) throws Exception;

	/**
	 * adds the specified video to Stock
	 * 
	 * @param video
	 *            the video to be added
	 */
	void addVideo(Video video) throws Exception;

	/**
	 * updates the article quantity into stock
	 * 
	 * @param article
	 *            the article to update
	 * @param qty
	 *            the new quantity
	 */
	void updateQuantityArticle(Article article, int qty) throws Exception;

}
