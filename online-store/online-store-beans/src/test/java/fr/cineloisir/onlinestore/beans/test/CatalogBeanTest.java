package fr.cineloisir.onlinestore.beans.test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import junit.framework.Assert;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.cineloisir.onlinestore.beans.catalog.CatalogBean;
import fr.cineloisir.onlinestore.beans.catalog.CatalogBeanRemote;
import fr.cineloisir.onlinestore.entity.catalog.Article;
import fr.cineloisir.onlinestore.entity.catalog.Video;

@RunWith(Arquillian.class)
public class CatalogBeanTest {

	private static Article article, article2, article3;
	private static Calendar calendar = Calendar.getInstance();

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@Inject
	private UserTransaction transaction;

	@EJB
	CatalogBeanRemote catalogBean;

	@Deployment
	public static Archive<?> createDeployment() {
		WebArchive archive = ShrinkWrap
				.create(WebArchive.class, "testcatalogbean.war")
				.addAsLibraries(
						Maven.resolver().loadPomFromFile("pom.xml")
								.resolve("fr.cineloisir:online-store-dao")
								.withTransitivity().asFile())
				.addPackage(CatalogBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return archive;
	}

	public static Date getZeroTimeDate(Date date) {
		Date res = date;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = calendar.getTime();

		return res;
	}

	public void setUp() throws Exception {
		Video video = new Video();
		video.setDirector("dir2"); //$NON-NLS-1$
		video.setGenre("action"); //$NON-NLS-1$
		video.setRunningTime(90);
		video.setTitle("title4"); //$NON-NLS-1$
		manager.persist(video);
		article = new Article();
		article.setFormat("dvd"); //$NON-NLS-1$
		article.setUnitCost(15);
		article.setNbUnits(200);
		article.setNbSales(120);
		article.setCatalogueDate(new Date());
		article.setVideo(video);
		manager.persist(article);

		Video video2 = new Video();
		video2.setDirector("dir"); //$NON-NLS-1$
		video2.setGenre("drame"); //$NON-NLS-1$
		video2.setRunningTime(90);
		video2.setTitle("title5"); //$NON-NLS-1$
		manager.persist(video2);
		calendar.set(2014, 8, 7);
		article2 = new Article();
		article2.setFormat("blueRay"); //$NON-NLS-1$
		article2.setUnitCost(15);
		article2.setNbUnits(200);
		article2.setNbSales(100);
		article2.setCatalogueDate(calendar.getTime());
		article2.setVideo(video2);
		manager.persist(article2);

		Video video3 = new Video();
		video3.setDirector("dir2"); //$NON-NLS-1$
		video3.setGenre("action"); //$NON-NLS-1$
		video3.setRunningTime(90);
		video3.setTitle("title4"); //$NON-NLS-1$
		manager.persist(video3);
		article3 = new Article();
		article3.setFormat("dvd"); //$NON-NLS-1$
		article3.setUnitCost(15);
		article3.setNbUnits(200);
		article3.setNbSales(150);
		article3.setCatalogueDate(new Date());
		article3.setVideo(video3);
		manager.persist(article3);
	}

	@Before
	public void beginTransaction() throws Exception {
		this.transaction.begin();
		this.manager.joinTransaction();
		setUp();
		this.transaction.commit();
	}

	@Test
	public void findByTitleTest() throws Exception {
		List<Article> articles = catalogBean.findArticlesByTitle("title4");
		/*
		 * Tests if all articles returned have the specified title
		 */
		for (Article a : articles) {
			Assert.assertEquals("", "title4", a.getVideo().getTitle());
		}
	}

	@Test
	public void findArticlesByGenreTest() throws Exception {
		List<Article> articles = catalogBean.findArticlesByGenre("action");
		/*
		 * Tests if all articles returned have the specified genre
		 */
		for (Article a : articles) {
			Assert.assertEquals("", "action", a.getVideo().getGenre());
		}

	}

	@Test
	public void findArticlesByDirectorTest() throws Exception {
		List<Article> articles = catalogBean.findArticlesByDirector("dir2");
		/*
		 * Tests if all articles returned have the specified director
		 */
		for (Article a : articles) {
			Assert.assertEquals("", "dir2", a.getVideo().getDirector());
		}
	}

	@Test
	public void findArticlesByReleaseYearTest() throws Exception {
		List<Article> articles2016 = catalogBean
				.findArticlesByReleaseYear(2016);
		Assert.assertEquals("", 0, articles2016.size());
		List<Article> articles = catalogBean.findArticlesByReleaseYear(2014);
		Calendar calendar = Calendar.getInstance();
		/*
		 * Tests if all articles returned have the specified release year
		 */
		for (Article a : articles) {
			calendar.setTime(a.getCatalogueDate());
			Assert.assertEquals("", 2014, calendar.get(Calendar.YEAR));
		}
	}

	@Test
	public void findBestSellingArticlesTest() throws Exception {
		catalogBean.setNbTop(2);
		List<Article> articles = catalogBean.findBestSellingArticles();
		/*
		 * Tests if all articles returned have the the highest number of sales
		 */
		Assert.assertEquals("", 2, articles.size());
		Assert.assertTrue("", articles.get(0).getNbSales() > 100);
		Assert.assertTrue("", articles.get(1).getNbSales() > 100);
	}

	@Test
	public void findNewArticlesTest() throws Exception {
		catalogBean.setNbTop(1);
		List<Article> articles = catalogBean.findNewArticles();
		/*
		 * Tests if article returned have the most new date
		 */
		Assert.assertEquals("", 1, articles.size());
		Assert.assertTrue("",
				getZeroTimeDate(articles.get(0).getCatalogueDate()).equals(
						getZeroTimeDate(calendar.getTime())));
	}
}
