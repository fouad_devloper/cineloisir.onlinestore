package fr.cineloisir.onlinestore.beans.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.cineloisir.onlinestore.beans.customers.CustomerBean;
import fr.cineloisir.onlinestore.beans.customers.CustomerBeanRemote;
import fr.cineloisir.onlinestore.entity.customer.Customer;

@RunWith(Arquillian.class)
public class CustomerBeanTest {

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@EJB
	private CustomerBeanRemote customerBean;

	@Deployment
	public static Archive<?> getDeployment() {
		WebArchive archive = ShrinkWrap
				.create(WebArchive.class, "testStockbean.war")
				.addAsLibraries(
						Maven.resolver().loadPomFromFile("pom.xml")
								.resolve("fr.cineloisir:online-store-dao")
								.withTransitivity().asFile())
				.addPackage(CustomerBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return archive;
	}

	@Before
	public void setUp() throws Exception {
		if (this.manager.find(Customer.class, 1) == null) {
			Customer customer = new Customer();
			customer.setFirstName("firstName1"); //$NON-NLS-1$
			customer.setLastName("lastName1"); //$NON-NLS-1$
			customer.setLogin("login1"); //$NON-NLS-1$
			customer.setPassword("password1"); //$NON-NLS-1$
			customer.setMail("mail1@gmail.com"); //$NON-NLS-1$
			customer.setStreet1("street1"); //$NON-NLS-1$
			customer.setStreet2("street2"); //$NON-NLS-1$
			customer.setZipCode(44400);
			customer.setCity("Nantes"); //$NON-NLS-1$
			this.customerBean.createCustomer(customer);
		}

	}

	@Test
	public void CreateCustomerTest() throws Exception {
		/*
		 * Tests if the customer was inserted in the database with the correct
		 * login and password
		 */
		Customer myCustomer = this.manager.find(Customer.class, 1);
		assertNotNull(
				"The customer was not inserted in the database", myCustomer); //$NON-NLS-1$
		assertEquals(
				"The customer inserted in the database has not the correct login ", "login1", myCustomer.getLogin()); //$NON-NLS-1$ //$NON-NLS-2$
		assertEquals(
				"The customer inserted in the database has not the correct password ", "password1", myCustomer.getPassword()); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Test
	public void authenticateTest() throws Exception {
		/*
		 * Tests if a valid user can be authenticated
		 */
		Customer myCustomer = this.customerBean.authenticate("login1",
				"password1");
		assertNotNull("a valid user can not be authenticated", myCustomer); //$NON-NLS-1$
		myCustomer = this.customerBean.authenticate("login2", "password2");
		assertNull("an invalid user can be authenticated", myCustomer); //$NON-NLS-1$
	}

	@Test
	public void updateCustomerAddressTest() throws Exception {
		/*
		 * Tests if the customer city,street1,street2,zip code were updated in
		 * the database with the correct value
		 */
		Customer myCustomer = this.manager.find(Customer.class, 1);
		assertEquals("", "Nantes", myCustomer.getCity());
		assertEquals("", 44400, myCustomer.getZipCode());
		assertEquals("", "street1", myCustomer.getStreet1());
		assertEquals("", "street2", myCustomer.getStreet2());

		this.customerBean.updateAdresseCustomer(myCustomer, "Nantes2",
				"street12", "street22", 44000);
		myCustomer = this.manager.find(Customer.class, 1);
		assertEquals("the customer city was not updated", "Nantes2",
				myCustomer.getCity());
		assertEquals("the customer zip code was not updated", 44000,
				myCustomer.getZipCode());
		assertEquals("the customer street1 was not updated", "street12",
				myCustomer.getStreet1());
		assertEquals("the customer street2 was not updated", "street22",
				myCustomer.getStreet2());
	}

	@Test
	public void updateCustomerMailTest() throws Exception {
		/*
		 * Tests if the customer email was updated in the database with the
		 * correct value
		 */
		Customer myCustomer = this.manager.find(Customer.class, 1);
		assertEquals("", "mail1@gmail.com", myCustomer.getMail());
		this.customerBean.updateEmailCustomer(myCustomer, "mail2@gmail.com");
		myCustomer = this.manager.find(Customer.class, 1);
		assertEquals("the customer email was not updated", "mail2@gmail.com",
				myCustomer.getMail());
	}
}
