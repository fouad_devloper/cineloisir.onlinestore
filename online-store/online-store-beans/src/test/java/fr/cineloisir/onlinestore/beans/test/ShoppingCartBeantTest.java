package fr.cineloisir.onlinestore.beans.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.cineloisir.onlinestore.beans.shoppingcart.ShoppingCartBean;
import fr.cineloisir.onlinestore.beans.shoppingcart.ShoppingCartBeanRemote;
import fr.cineloisir.onlinestore.beans.stock.StockBean;
import fr.cineloisir.onlinestore.beans.stock.StockBeanRemote;
import fr.cineloisir.onlinestore.entity.catalog.Article;
import fr.cineloisir.onlinestore.entity.catalog.Video;

@RunWith(Arquillian.class)
public class ShoppingCartBeantTest {

	@Deployment
	public static Archive<?> createDeployement() {
		WebArchive archive = ShrinkWrap
				.create(WebArchive.class, "ShoppingCartBeantbean.war")
				.addAsLibraries(
						Maven.resolver().loadPomFromFile("pom.xml")
								.resolve("fr.cineloisir:online-store-dao")
								.withTransitivity().asFile())
				.addPackage(ShoppingCartBean.class.getPackage())
				.addPackage(StockBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return archive;
	}

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@EJB
	private StockBeanRemote stockBean;

	@EJB
	private ShoppingCartBeanRemote shoppingCartBean;

	@Before
	public void setUp() throws Exception {
		if (this.manager.find(Video.class, 1) == null) {
			Video video = new Video();
			video.setDirector("dir1"); //$NON-NLS-1$
			video.setGenre("action,aventure"); //$NON-NLS-1$
			video.setRunningTime(136);
			video.setTitle("title1"); //$NON-NLS-1$
			this.stockBean.addVideo(video);
		}

	}

	@Test
	public void validateCartTest() throws Exception {
		// article1
		Article article = new Article();
		article.setFormat("dvd"); //$NON-NLS-1$
		article.setUnitCost(15);
		article.setNbUnits(200);
		article.setNbSales(100);
		article.setCatalogueDate(new Date());
		article.setVideo(manager.find(Video.class, 1));
		this.stockBean.addArticle(article);
		// article2
		Article article2 = new Article();
		article2.setFormat("dvd"); //$NON-NLS-1$
		article2.setUnitCost(15);
		article2.setNbUnits(50);
		article2.setNbSales(20);
		article2.setCatalogueDate(new Date());
		article2.setVideo(manager.find(Video.class, 1));
		this.stockBean.addArticle(article2);

		Article myArticle = this.manager.find(Article.class, 1);
		Article myArticle2 = this.manager.find(Article.class, 2);
		this.shoppingCartBean.addItem(myArticle, 30);
		this.shoppingCartBean.addItem(myArticle2, 20);
		this.shoppingCartBean.validateCart();
		/*
		 * Tests if the article number of units and number of sales were updated
		 */
		myArticle = this.manager.find(Article.class, 1);
		myArticle2 = this.manager.find(Article.class, 2);
		assertEquals("article: number of units was not updated", 170,
				myArticle.getNbUnits());
		assertEquals("article: number of sales was not updated", 130,
				myArticle.getNbSales());
		assertEquals("article2: number of units was not updated", 30,
				myArticle2.getNbUnits());
		assertEquals("article2: number of sales was not updated", 40,
				myArticle2.getNbSales());

	}

}
