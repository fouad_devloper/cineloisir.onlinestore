package fr.cineloisir.onlinestore.beans.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.cineloisir.onlinestore.beans.stock.StockBean;
import fr.cineloisir.onlinestore.beans.stock.StockBeanRemote;
import fr.cineloisir.onlinestore.entity.catalog.Article;
import fr.cineloisir.onlinestore.entity.catalog.Video;

@RunWith(Arquillian.class)
public class StockBeanTest {

	@PersistenceContext(unitName = "online-store-dao")
	private EntityManager manager;

	@EJB
	private StockBeanRemote stockBean;

	@Deployment
	public static Archive<?> createDeployement() {
		WebArchive archive = ShrinkWrap
				.create(WebArchive.class, "testStockbean.war")
				.addAsLibraries(
						Maven.resolver().loadPomFromFile("pom.xml")
								.resolve("fr.cineloisir:online-store-dao")
								.withTransitivity().asFile())
				.addPackage(StockBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return archive;
	}

	@Before
	public void setUp() throws Exception {
		if (this.manager.find(Video.class, 1) == null) {
			Video video = new Video();
			video.setDirector("dir1"); //$NON-NLS-1$
			video.setGenre("action,aventure"); //$NON-NLS-1$
			video.setRunningTime(136);
			video.setTitle("title1"); //$NON-NLS-1$
			this.stockBean.addVideo(video);
		}

	}

	@Test
	public void addVideoTest() throws Exception {
		Video video = new Video();
		video.setDirector("dir2"); //$NON-NLS-1$
		video.setGenre("action,aventure"); //$NON-NLS-1$
		video.setRunningTime(136);
		video.setTitle("title2"); //$NON-NLS-1$
		this.stockBean.addVideo(video);
		/*
		 * Tests if the video was inserted in the database with the correct
		 * title
		 */
		Video myVideo = this.manager.find(Video.class, 2);
		assertNotNull("The video was not inserted in the database", myVideo); //$NON-NLS-1$
		assertEquals(
				"The video inserted in the database has not the correct title ", "title2", myVideo.getTitle()); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Test
	public void AddArticleTest() throws Exception {
		Article article = new Article();
		article.setFormat("dvd"); //$NON-NLS-1$
		article.setUnitCost(15);
		article.setNbUnits(200);
		article.setNbSales(100);
		article.setCatalogueDate(new Date());
		article.setVideo(manager.find(Video.class, 1));
		this.stockBean.addArticle(article);
		/*
		 * Tests if the article was inserted in the database with the correct
		 * video
		 */
		Article myArticle = this.manager.find(Article.class, 1);
		assertNotNull("The article was not inserted in the database", myArticle); //$NON-NLS-1$
		assertEquals(
				"The article inserted in the database has not the correct video ", 1, myArticle.getVideo().getId()); //$NON-NLS-1$
	}

	@Test
	public void updateQuantityArticleTest() throws Exception {
		Article article = new Article();
		article.setFormat("dvd"); //$NON-NLS-1$
		article.setUnitCost(15);
		article.setNbUnits(0);
		article.setNbSales(100);
		article.setCatalogueDate(new Date());
		article.setVideo(manager.find(Video.class, 1));
		this.stockBean.addArticle(article);
		/*
		 * Tests if the article quantity was updated in the database with the
		 * correct value
		 */
		Article myArticle = this.manager.find(Article.class, 2);
		assertEquals("", 0, myArticle.getNbUnits());
		this.stockBean.updateQuantityArticle(myArticle, 300);
		myArticle = this.manager.find(Article.class, 2);
		assertEquals("the article quantity was not updated", 300,
				myArticle.getNbUnits());
	}


}
