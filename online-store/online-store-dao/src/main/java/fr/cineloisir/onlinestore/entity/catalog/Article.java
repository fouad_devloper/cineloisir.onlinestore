package fr.cineloisir.onlinestore.entity.catalog;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_Article")
public class Article implements Serializable {

	public Article() {
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@OneToOne
	@JoinColumn(nullable=false)
	private Video video;
	@Column(nullable=false)
	private String format;
	@Column(nullable=false)
	private int unitCost;
	@Column(nullable=false)
	@Temporal(TemporalType.DATE)
	private Date catalogueDate;
	@Column(nullable=false)
	private int nbUnits ;
	@Column(nullable=false)
	private int nbSales;
	
	public Video getVideo() {
		return this.video;
	}
	public void setVideo(Video video) {
		this.video = video;
	}
	public String getFormat() {
		return this.format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public int getUnitCost() {
		return this.unitCost;
	}
	public void setUnitCost(int unitCost) {
		this.unitCost = unitCost;
	}
	public Date getCatalogueDate() {
		return this.catalogueDate;
	}
	public void setCatalogueDate(Date catalogueDate) {
		this.catalogueDate = catalogueDate;
	}
	public int getNbUnits() {
		return this.nbUnits;
	}
	public void setNbUnits(int nbUnits) {
		this.nbUnits = nbUnits;
	}
	public int getNbSales() {
		return this.nbSales;
	}
	public void setNbSales(int nbSales) {
		this.nbSales = nbSales;
	}
	public int getId() {
		return this.id;
	}
}
