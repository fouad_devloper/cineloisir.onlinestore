package fr.cineloisir.onlinestore.entity.catalog;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_Video")
public class Video implements Serializable {
	
	public Video() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable=false, length=45)
	private String title;
	@Column(nullable=false, length=45)
	private String Director;
	@Column(nullable=false)
	private int runningTime;
	@Column(nullable=false)
	private String genre;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return this.Director;
	}

	public void setDirector(String director) {
		this.Director = director;
	}

	public int getRunningTime() {
		return this.runningTime;
	}

	public void setRunningTime(int runningTime) {
		this.runningTime = runningTime;
	}

	public String getGenre() {
		return this.genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getId() {
		return this.id;
	}


}
