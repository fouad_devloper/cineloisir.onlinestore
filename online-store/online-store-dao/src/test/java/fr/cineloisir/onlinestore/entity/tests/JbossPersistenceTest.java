package fr.cineloisir.onlinestore.entity.tests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.cineloisir.onlinestore.entity.catalog.Article;
import fr.cineloisir.onlinestore.entity.catalog.Video;
import fr.cineloisir.onlinestore.entity.customer.Customer;

@RunWith(Arquillian.class)
public class JbossPersistenceTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "testPersistence.war") //$NON-NLS-1$
            .addClass(JbossPersistenceTest.class)
            .addPackage(Article.class.getPackage())
            .addPackage(Customer.class.getPackage())
            .addAsResource("META-INF/persistence.xml") //$NON-NLS-1$
            .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml"); //$NON-NLS-1$
    }
	
  @PersistenceContext(unitName="online-store-dao")
	 EntityManager manager;
	
	@Inject
	 UserTransaction transaction;
	
	@Before
	public void beginTransaction() throws Exception {
	    this.transaction.begin();
	    this.manager.joinTransaction();
	}
	
	@SuppressWarnings({ "boxing" })
	@Test
	public void VideoPersistenceTest() throws Exception {
		Video video=new Video();
		video.setDirector("dir1"); //$NON-NLS-1$
		video.setGenre("action,aventure"); //$NON-NLS-1$
		video.setRunningTime(136);
		video.setTitle("title1"); //$NON-NLS-1$
		this.manager.persist(video);
		this.transaction.commit();
		/*
		 * Tests if the video is inserted in the database with the correct title
		 */
		int videoId = video.getId();
		assertNotNull("Video Id not null", videoId); //$NON-NLS-1$
		Video myVideo = this.manager.find(Video.class, videoId);
		assertNotNull("The video was not inserted in the database", myVideo); //$NON-NLS-1$
		assertEquals("The video inserted in the database has not the correct title ", "title1", myVideo.getTitle()); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	@SuppressWarnings({ "boxing" })
	@Test
	public void ArticlePersistenceTest() throws Exception {
		Video video=new Video();
		video.setDirector("dir2"); //$NON-NLS-1$
		video.setGenre("action,aventure"); //$NON-NLS-1$
		video.setRunningTime(90);
		video.setTitle("title2"); //$NON-NLS-1$
		this.manager.persist(video);
		Article article=new Article();
		article.setFormat("dvd"); //$NON-NLS-1$
		article.setUnitCost(15);
		article.setNbUnits(200);
		article.setNbSales(100);
		article.setCatalogueDate(new Date());
		article.setVideo(video);
		this.manager.persist(article);
		this.transaction.commit();
		/*
		 * Tests if the article is inserted in the database with the correct
		 * video
		 */
		int articleId=article.getId();
		assertNotNull("Article Id not null",articleId); //$NON-NLS-1$
		Article myArticle=this.manager.find(Article.class,articleId);
		assertNotNull("The article was not inserted in the database",myArticle); //$NON-NLS-1$
		assertEquals(
				"The article inserted in the database has not the correct video ", video.getId(), myArticle.getVideo().getId()); //$NON-NLS-1$
	}
	
	@SuppressWarnings({ "boxing" })
	@Test
	public void CustomerPersistenceTest() throws Exception {
		Customer customer=new Customer();
		customer.setFirstName("firstName1"); //$NON-NLS-1$
		customer.setLastName("lastName1"); //$NON-NLS-1$
		customer.setLogin("login1"); //$NON-NLS-1$
		customer.setPassword("password1"); //$NON-NLS-1$
		customer.setMail("mail1@gmail.com"); //$NON-NLS-1$
		customer.setStreet1("street1"); //$NON-NLS-1$
		customer.setStreet2("street2"); //$NON-NLS-1$
		customer.setZipCode(44400);
		customer.setCity("Nantes"); //$NON-NLS-1$
		this.manager.persist(customer);
		this.transaction.commit();
		/*
		 * Tests if the customer is inserted in the database with the correct
		 * login and password
		 */
		int customerId=customer.getId();
		assertNotNull("Customer Id not null",customerId); //$NON-NLS-1$
		Customer myCustomer=this.manager.find(Customer.class, customerId);
		assertNotNull("The customer was not inserted in the database",myCustomer); //$NON-NLS-1$
		assertEquals("The customer inserted in the database has not the correct login ", "login1", myCustomer.getLogin()); //$NON-NLS-1$ //$NON-NLS-2$
		assertEquals("The customer inserted in the database has not the correct password ", "password1", myCustomer.getPassword()); //$NON-NLS-1$ //$NON-NLS-2$
		
	}
	
	

}
