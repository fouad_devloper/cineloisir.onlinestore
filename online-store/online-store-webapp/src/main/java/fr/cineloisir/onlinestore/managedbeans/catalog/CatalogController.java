package fr.cineloisir.onlinestore.managedbeans.catalog;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.cineloisir.onlinestore.beans.catalog.CatalogBeanRemote;
import fr.cineloisir.onlinestore.entity.catalog.Article;

@ManagedBean
@SessionScoped
public class CatalogController {
	private String genre;
	private String director;
	private String title;
	private int releaseYear;
	private List<Article> articles;

	@EJB
	CatalogBeanRemote catalogBean;

	public String findArticlesByGenre() {
		this.articles = this.catalogBean.findArticlesByGenre(this.genre);
		return null;
	}

	public String findArticlesByDirector() {
		this.articles = this.catalogBean.findArticlesByDirector(this.director);
		return null;
	}

	public String findArticlesByReleaseYear() {
		this.articles = this.catalogBean
				.findArticlesByReleaseYear(this.releaseYear);
		return null;
	}

	public String findArticlesByTitle() {
		this.articles = this.catalogBean.findArticlesByTitle(this.title);
		return null;
	}

	public String findBestSellingArticles() {
		this.articles = this.catalogBean.findBestSellingArticles();
		return "films";
	}

	public String findNewArticles() {
		this.articles = this.catalogBean.findNewArticles();
		return "films";
	}

	public String findAllArticles() {
		this.articles = this.catalogBean.findAllArticles();
		return "films";
	}

	public String getGenre() {
		return genre;
	}


	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
