package fr.cineloisir.onlinestore.managedbeans.customer;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.cineloisir.onlinestore.beans.customers.CustomerBeanRemote;
import fr.cineloisir.onlinestore.entity.customer.Customer;

public class CustomerController {

	@EJB
	CustomerBeanRemote customerBean;

	private Customer customer = new Customer();
	private String userName;
	private String password;

	public String authenticate() {
		this.customer = this.customerBean.authenticate(userName, password);
		return null;
	}

	public String createCustomer() throws Exception {
		this.customerBean.createCustomer(this.customer);
		return null;
	}

	public String updateCustomer() throws Exception {
		this.customerBean.updateAdresseCustomer(this.customer,
				this.customer.getCity(), this.customer.getStreet1(),
				this.customer.getStreet2(), this.customer.getZipCode());
		this.customerBean.updateEmailCustomer(this.customer,
				this.customer.getMail());
		return null;
	}

	public String signOff() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(false);
		session.invalidate();
		return null;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
