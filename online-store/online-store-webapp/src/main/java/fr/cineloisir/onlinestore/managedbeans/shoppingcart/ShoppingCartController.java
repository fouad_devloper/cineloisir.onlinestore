package fr.cineloisir.onlinestore.managedbeans.shoppingcart;

import javax.ejb.EJB;

import fr.cineloisir.onlinestore.beans.shoppingcart.ShoppingCartBeanRemote;

public class ShoppingCartController {

	@EJB
	ShoppingCartBeanRemote shoppingCartBean;

	public String vaildateCart() throws Exception {
		this.shoppingCartBean.validateCart();
		return null;
	}
}
